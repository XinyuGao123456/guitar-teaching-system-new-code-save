import os

from django.shortcuts import render
from django.http import HttpResponse
from .models import Student
from django.views.decorators.csrf import csrf_exempt
import json

all_data = []


def index(request):

    return render(request, 'pages/index.html')


def about(request):
    return render(request, 'pages/about.html')


def students(request):
    students = Student.objects.all()
    context = {
        'students': students,
        'islogin': 'true'
    }
    return render(request, 'pages/students.html', context)


def student(request, student_id):
    return render(request, 'pages/student.html')


def courses(request):
    return render(request, 'pages/courses.html')

@csrf_exempt
def uploadImage(request):
    image = request.FILES.get("file")
    #dir = "D:\\a.jpg" lightEventData.json
# "C:\\Users\\Administrator\\Desktop\\Project_Guitar\\guitar-teaching-system\\guitar\\media\\a.jpg"
    file_name = "a.jpg"  # 覆盖变量
    base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))  # 项目根目录
    file_path = os.path.join(base_dir, 'media', file_name)  # 下载文件的绝对路径
    with open(file_path, 'wb') as f:
        f.write(image.read())
        f.close()
    return HttpResponse("success")


@csrf_exempt
def saveJsonData(request):
    save_data_list = [
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0]
                ]

    data_list = request.POST.get('list')
    json_data = json.loads(data_list)
    print(data_list)
    print(json_data[0])
    print(type(json_data[0]))
    print(json_data[0]['row'])
    print(len(json_data))
    for i in range(len(json_data)):
        print(i)
        save_data_list[json_data[i]['row']][json_data[i]['col']] = int(json_data[i]['fingerId'])
    dict_1 = {"finger_data":save_data_list, "durationTime":int(json_data[0]['duration'])}
    all_data.append(dict_1)
    print(all_data)
    #print(data_list)

    #save_data_list.append()
    return HttpResponse("success")

@csrf_exempt
def saveAsJsonFile(request):
    aLargeDict = {"positions": all_data}
    jsondata = json.dumps(aLargeDict, indent=4, separators=(',', ': '))
    print(all_data)
    all_data.clear()
    print(all_data)
    file_name = 'lightEventData.json'
    base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))  # 项目根目录
    file_path = os.path.join(base_dir, 'media', file_name)  # 下载文件的绝对路径
    f = open(file_path, 'w')
    f.write(jsondata)
    f.close()
    return HttpResponse("success")

@csrf_exempt
def loadJsonToPage(request):
    with open("D:\\lightEventData.json", 'r', encoding='utf8')as fp:
        # json_data = json.load(fp)
        # json_data1 = json.dumps(json_data)
        # print(json_data1)
        # str1 = str(json_data['light_event_data'])
        # str1 = json_data
        # print('这是文件中的json数据：', json_data)
        # print('这是读取到文件数据的数据类型：', type(json_data))
        return HttpResponse(fp)


@csrf_exempt
def addstudent(request):
    name = request.POST.get("name")
    de = request.POST.get("de")
    level = request.POST.get("level")
    email = request.POST.get("email")
    print(name)
    user = Student(
        name=name,
        description=de,
        email=email,
        level=level)
    # Login after register
    # auth.login(request, user)
    # messages.success(request, 'You are now log in')
    # return redirect('index')
    user.save()
    return HttpResponse("success")




@csrf_exempt
def deletestudent(request):
    if request.POST:
        id = request.POST.get('id')
        Student.objects.filter(id=id).delete()
        return HttpResponse("success")
    else:
        return HttpResponse("error")

@csrf_exempt
def editstudent(request):
    id = request.POST.get("id")
    name = request.POST.get("name")
    de = request.POST.get("de")
    level = request.POST.get("level")
    email = request.POST.get("email")
    print(id)
    Student.objects.filter(id=id).update(name=name,description=de,level=level,email=email)

    return HttpResponse("success")





from django.shortcuts import render, HttpResponse
from django.http import StreamingHttpResponse
import os


# Create your views here.
def img_file_down(request, id):
    data = [{"id": "1", "image": "a.jpg"}]
    file_name = ""
    for i in data:
        if i["id"] == id:
            file_name = i["image"]

    base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    file_path = os.path.join(base_dir, 'media', file_name)
    print(file_path)

    if not os.path.isfile(file_path):
        return HttpResponse("Sorry but Not Found the File")

    def file_iterator(file_path, chunk_size=512):
        with open(file_path, mode='rb') as f:
            while True:
                c = f.read(chunk_size)
                if c:
                    yield c
                else:
                    break

    try:
        response = StreamingHttpResponse(file_iterator(file_path))
        response['Content-Type'] = 'application/octet-stream'
        response['Content-Disposition'] = 'attachment;filename="{}"'.format(file_name)
    except:
        return HttpResponse("Sorry but Not Found the File")

    return response







def json_file_down(request, id):
    data = [{"id": "2", "image": "lightEventData.json"}]
    file_name = ""
    for i in data:
        if i["id"] == id:
            file_name = i["image"]
    file_name = "lightEventData.json"
    base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    file_path = os.path.join(base_dir, 'media', file_name)
    print(file_path)
    print(file_name)
    if not os.path.isfile(file_path):
        return HttpResponse("Sorry but Not Found the File")

    def file_iterator(file_path, chunk_size=512):
        with open(file_path, mode='rb') as f:
            while True:
                c = f.read(chunk_size)
                if c:
                    yield c
                else:
                    break

    try:
        response = StreamingHttpResponse(file_iterator(file_path))
        response['Content-Type'] = 'application/octet-stream'
        response['Content-Disposition'] = 'attachment;filename="{}"'.format(file_name)
    except:
        return HttpResponse("Sorry but Not Found the File")

    return response