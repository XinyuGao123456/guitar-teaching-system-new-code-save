from django.urls import path, re_path
from . import views
from .custom_api_views import TouchView
from .custom_api_views import LightView
from django.contrib import admin

urlpatterns = [
    path('', views.index, name='index'),
    # path('about/', views.about, name='about'),
    path('students/', views.students, name='students'),
    # path('courses/', views.courses, name='courses'),
    path('<int:student_id>', views.student, name='student'),
    path('touchEventData/', TouchView.as_view(), name='touch'),
    path('lightEventData/', LightView.as_view(), name='light'),
    path('uploadImage/', views.uploadImage),
    path('saveJsonData/', views.saveJsonData),
    path('saveAsJsonFile/', views.saveAsJsonFile),
    path('loaddata/', views.loadJsonToPage),
    path('addstudent/', views.addstudent),
    path('editstudent/', views.editstudent),
    path('deleteStudent/', views.deletestudent),
    path('admin/', admin.site.urls),
    path('', views.index),
    re_path('download/(?P<id>\d+)', views.img_file_down, name="download"),
    re_path('download1/(?P<id>\d+)', views.json_file_down, name="download"),
]