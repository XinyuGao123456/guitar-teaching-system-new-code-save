// colors[0]: index finger; colors[0]: middle finger; colors[0]: ring finger; colors[0]: little thumb;
const colors = ["#7bdff2", "#fcf6bd", "#f7d6e0", "#b2f7ef"]
const fingers = ['Index Finger', 'Middle Finger', 'Ring Finger', 'Little Thumb']
const colorsBorder = ["#0000ff", "#ffff00", "#ff0000", "#009900"]

// Define table as the guitar board
const table = $("table tr")


// Define buttonRow and buttonCol to record which grid the teacher gives instructions to
let buttonRow
let buttonCol

// Button part
// Define a Button class, with row and col properties
class Button {
  constructor(row, col, button) {
    this.row = row
    this.col = col
    this.button = button
  }
}

// Create buttons as a 2-dimensional array to store 6*4=24 grids
let buttons = new Array(6)
for(let i = 0; i < 6; i ++) {
  buttons[i] = new Array(4)
}

// Create button objects and add event listener to each button -- return the position -- row and col
function createButtonsArray(){
  for(let row = 0; row < 6; row ++) {
    for(let col = 0; col < 4; col ++) {
      let btn = buttons[row][col]
      btn = new Button(row, col, table.eq(row).find("td").eq(col).find("button"))
      btn.button.on("click", () => {
        buttonRow = btn.row
        buttonCol = btn.col
        //prompt("you have chosen this grid, and please fill the form blow to show your instruction")
      })
      // buttons[row][col] = new Button(row, col, table.eq(row).find("td").eq(col).find("button"))
    }
  }
}
createButtonsArray()
console.log(buttons)

// Add event listener for every button
// function buttonsOnClick(){
//   for(let i = 0; i < 6; i ++){
//     for (let j = 0; j < 4; j ++){
//       let btn = buttons[i][j]
//       btn.button.on("click", () => {
//         buttonRow = btn.row
//         buttonCol = btn.col
//       })
//     }
//   }
// }
// buttonsOnClick()



// Instructions Part 🦊🦊🦊
let fingerId = document.querySelector("#finger_id")
let lastingTime = document.querySelector("#duration")

// Create a list to store all the instructions (there could be 1 grid, 2 grids and so on
class Instruction {
  constructor(fingerId, duration, row, col) {
    this.fingerId = fingerId
    this.duration = duration
    this.row = buttonRow
    this.col = buttonCol
  }
}
let instructionsList = []

// addBtn: click to add teacher's instruction and store the instructions in a list(instructionList)
const addBtn = $("#add-info")
addBtn.on("click", (e) => {
  instructionsList.push(new Instruction(fingerId.value, lastingTime.value))
  e.preventDefault()
    alert(fingerId.value)
    alert(lastingTime.value)
    show()
})

// submitBtn: click to show teacher's all instructions
const submitBtn = $("#submit-info")
submitBtn.on("click", show)

function showInstructions(list) {
  let row, col, num, lasting
  for(let instruction of list) {
    row = instruction.row
    col = instruction.col
    num = instruction.fingerId
    lasting = instruction.duration

    changeInfo(lasting, row, col, num)
  }

  // instructionsList = []
  // e.preventDefault()
}
function saveList(){
    submitList(instructionsList)
}
function submitList(list){
    var str = JSON.stringify(list);
      $.ajax({
   url:"saveJsonData/",
   type:"POST",

   data:{ list:str },
   success:function(data){
    if(data == "success"){
     alert("success")
    }

    else{
     alert("false")
    }
    instructionsList = []
       reset()
   },
   fail:function(data){
    alert("false")
   }
  })
}


function saveAsJsonFile(){
          $.ajax({
   url:"saveAsJsonFile/",
   type:"POST",

   success:function(data){
    if(data == "success"){
     alert("success")
    }

    else{
     alert("false")
    }
   },
   fail:function(data){
    alert("false")
   }
  })
}
function changeInfo(duration, rowIndex, colIndex, num) {
  let button = table
        .eq(rowIndex).find("td").eq(colIndex).find("button")
        .css("border-color", colorsBorder[num])
        .text(fingers[num] + "; " + duration + "s")
    return button
}

function show(){
  showInstructions(instructionsList)
}





// Util functions 🥥🥥🥥
function changeColor(rowIndex, colIndex, num, type) {
  if(type === 0){
    let button = table
        .eq(rowIndex).find("td").eq(colIndex).find("button")
        .css("background-color", colors[num])
        .text(fingers[num])
    return button
  }
  if(type === 1) {
    let button = table
        .eq(rowIndex).find("td").eq(colIndex).find("button")
        .css("border-color", colorsBorder[num])
        // .text(fingers[num])
    return button
  }
}


function resetColor(type){
  if(type === 0) return table.find("button").css("background-color", "#efefef").text('')
  if(type === 1) return table.find("button").css("border-color", "#efefef").text('')
}


// Touch & Light Event 🦊🦊🦊
/*
    Display students playing process
 */
function touchColor() {
  const xhr = new XMLHttpRequest();
  xhr.open("GET", "http://127.0.0.1:8000/touchEventData", true);
  xhr.onload = function () {
    if (this.status === 200) {
      const sounds = JSON.parse(this.responseText);
      let preRow
      let preCol
      let sumTime = 0

      for (let i = 0; i < sounds.length; i++) {
        let data = sounds[i].finger_data
        let time = sounds[i].durationTime
        sumTime += time
        //reset
        setTimeout(() => {
          resetColor(0)
          for (let row = 0; row < 6; row++) {
            for (let col = 0; col < 4; col++) {
              if(data[row][col] !== 0){
                let colorNum = data[row][col] - 1
                changeColor(row, col, colorNum,0)
              }
            }
          }
        },sumTime-time)
      }
    }
  };
  xhr.send();
}


function lightColorJson(data1) {
    const sounds = JSON.parse(data1).light_event_data;
    let preRow
    let preCol
    let sumTime = 0
    for (let i = 0; i < sounds.length; i++) {
        let data = sounds[i].finger_data
        let time = sounds[i].durationTime
        sumTime += time
        //reset
        setTimeout(() => {
            resetColor(1)
            for (let row = 0; row < 6; row++) {
                for (let col = 0; col < 4; col++) {
                    if (data[row][col] !== 0) {
                        let colorNum = data[row][col] - 1
                        changeColor(row, col, colorNum, 1)
                    }
                }
            }
        }, sumTime - time)
    }
}

function touchColorJson(data1){

const sounds = JSON.parse(data1).light_event_data;
      let preRow
      let preCol
      let sumTime = 0

      for (let i = 0; i < sounds.length; i++) {
        let data = sounds[i].finger_data
        let time = sounds[i].durationTime
        sumTime += time
        //reset
        setTimeout(() => {
          resetColor(0)
          for (let row = 0; row < 6; row++) {
            for (let col = 0; col < 4; col++) {
              if(data[row][col] !== 0){
                let colorNum = data[row][col] - 1
                changeColor(row, col, colorNum,0)
              }
            }
          }
        },sumTime-time)
      }

}

/*
    Display teachers pre-set playing instructions
 */
function lightColor() {
  const xhr = new XMLHttpRequest();
  xhr.open("GET", "http://127.0.0.1:8000/lightEventData", true);
  xhr.onload = function () {
    if (this.status === 200) {
      const sounds = JSON.parse(this.responseText);
      let preRow
      let preCol
      let sumTime = 0

      for (let i = 0; i < sounds.length; i++) {
        let data = sounds[i].finger_data
        let time = sounds[i].durationTime
        sumTime += time
        //reset
        setTimeout(() => {
          resetColor(1)
          for (let row = 0; row < 6; row++) {
            for (let col = 0; col < 4; col++) {
              if(data[row][col] !== 0){
                let colorNum = data[row][col] - 1
                changeColor(row, col, colorNum,1)
              }
            }
          }
        },sumTime-time)
      }
    }
  };
  xhr.send();
}


function play(){
  //lightColor()
  //touchColor()
$.ajax({
   url:"loaddata/",
   type:"POST",
   success:function(data){
    alert(data)
    lightColorJson(data)
       touchColorJson(data)
   },
   fail:function(data){
    alert("false")
   }
  })
}


function reset(){
  resetColor(0)
  resetColor(1)
}


// Select buttons and and "click" event listener
$("#start-play").on("click", play);
$("#reset").on("click", reset);




// upload image instructions
const your_image = document.querySelector('#your_image');
let uploaded = ""
your_image.addEventListener('change', function() {
  const reader = new FileReader()
  reader.addEventListener('load', () => {
    uploaded = reader.result
    document.querySelector('#show_image').innerHTML = `
      <div id="display_image" style="background-image: url(${uploaded})">
      </div>
<!--      <button id="remove-image" class="btn btn-sample my-3">REMOVE</button>-->
    `

  })

  reader.readAsDataURL(this.files[0])

})



// const remove_image = document.querySelector('#remove-image')
// remove_image.on("click", () => {
//   document.querySelector('#show_image').innerHTML = `
//   <div></div>
//   `
// });

function upload(obj){
 var formData = new FormData();
    var files = obj.files;
    formData.append('file', obj.files[0]);  //添加图片信息的参数
    $.ajax({
        url:"uploadImage/",
        type: "POST",
        processData: false, // 告诉jQuery不要去处理发送的数据
        contentType: false, // 告诉jQuery不要去设置Content-Type请求头
        data: formData,
        success: function (data) {
          const your_image = document.querySelector('#your_image');
          let uploaded = ""
          your_image.addEventListener('change', function() {
          const reader = new FileReader()
          reader.addEventListener('load', () => {
          uploaded = reader.result
          document.querySelector('#show_image').innerHTML = `
            <div id="display_image" style="background-image: url(${uploaded})">
            </div>
<!--      <button id="remove-image" class="btn btn-sample my-3">REMOVE</button>-->
            `
  })

  reader.readAsDataURL(this.files[0])
  // $.post({
  //    url:"localhost:....",
  //    type:"POST",
  //    data:{
  //     locations, types, password,rentMoney
  //    },
  //    success:function(data){
  //     if(data == "success"){
  //      alert("Added successfully")
  //      //window.location.href = "../a_manage_bike/index"
  //     }
  //     else{
  //      alert("Error")
  //      }
  //    },
  //    fail:function(data){
  //     alert("false")
  //    }
  //   })
})

            alert(data);
        },
        fail: function (data) {
             alert("false")
        }
    })
}



function editStudent(id,name,description,level,email){
    $("#id1").val(id)
    $("#name1").val(name)
    $("#de1").val(description)
    $("#level1").val(level)
    $("#email1").val(email)
    // $("#hiddenEdit").click()
  $("#hiddenEdit").trigger("click")
}
function submiteditStudent(){
    var id = $("#id1").val();
    var name = $("#name1").val();
    var de = $("#de1").val();
    var level = $("#level1").val();
    var email = $("#email1").val();
    $.ajax({
        url: "../editstudent/",
        type:"POST",
        data: {
            id: id,
            name: name,
            de: de,
            level: level,
            email: email
        },
        success: function (data) {
            if (data == "success") {
                window.location.reload()
            }
        },
        fail: function (data) {
            alert("false")
        }
    })

}

function adStudent() {
    var name = $("#name").val();
    var de = $("#de").val();
    var level = $("#level").val();
    var email = $("#email").val();
    alert(de)
    $.ajax({
        url: "../addstudent/",
        type:"POST",
        data: {
            name: name,
            de: de,
            level: level,
            email: email
        },
        success: function (data) {
            if (data == "success") {
                alert("add success")
                window.location.reload()
            }
        },
        fail: function (data) {
            alert("false")
        }
    })
}

function deleteStudent(id){

    $.ajax({
        url: "../deleteStudent/",
        type:"POST",
        data: {
            id: id
        },
        success: function (data) {
            if (data == "success") {
                alert("delete success")
                window.location.reload()
            }
        },
        fail: function (data) {
            alert("false")
        }
    })
}
// document.getElementById('file').onchange = function() {
//     let imgFile = this.files[0];
//     let fr = new FileReader();
//     fr.onload = function() {
//         document.getElementById('image').getElementsByTagName('img')[0].src = fr.result;
//     };
//     fr.readAsDataURL(imgFile);
// };


// document
//     .querySelector('#imgPicker')
//     .addEventListener('change', function(){
//         //当没选中图片时，清除预览
//         if(this.files.length === 0){
//             document.querySelector('#preview').src = '';
//             return;
//         }
//
//         //实例化一个FileReader
//         let reader = new FileReader();
//
//         reader.onload = function (e) {
//             //当reader加载时，把图片的内容赋值给
//             document.querySelector('#preview').src = e.target.result;
//         };
//
//         //读取选中的图片，并转换成dataURL格式
//         reader.readAsDataURL(this.files[0]);
//     }, false);











// function checkIfCorrect(press, row, col, colorNum) {
//   if(row === correct[press][0] && col === correct[press][1]) {
//     changeColor(row, col, colors[colorNum]);
//     return true
//   }else {
//     changeColor(row, col, errorColor);
//     changeColor(correct[press][0], correct[press][1], correctColor)
//     return false
//   }
// }


// function playColor2() {
//   const xhr = new XMLHttpRequest();
//   xhr.open("GET", "http://127.0.0.1:8000/data", true);
//
//   xhr.onload = function () {
//     if (this.status === 200) {
//       const sounds = JSON.parse(this.responseText);
//       let preRow
//       let preCol
//       let error
//       let accuracy
//
//       for (let i = 0; i < sounds.length; i++) {
//         // let time = 100 * Math.ceil(Math.random()*10)  // set random time to simulate real playing process
//         setTimeout(() => {
//           let data = sounds[i].finger_data; // sound per time -- 6x4 array
//
//           for (let row = 0; row < 6; row++) {
//             for(let col = 0; col < 4; col ++){
//               if(data[row][col] !== 0){  // this string will being pushed
//                 if(i === 0) {
//                   preRow = row
//                   preCol = col
//                 }
//                 let colorNum = data[row][col] - 1
//                 if(checkIfCorrect(i, row, col, colorNum) === false) {
//                   error += 1
//                 }
//
//                 if(i !== 0) {
//                   setColorBack(preRow, preCol);
//                   preRow = row
//                   preCol = col
//                 }
//               }
//             }
//           }
// 		},1000 * i)  // press once per second
//       }
//       accuracy = error / sounds.length
//       console.log(accuracy)
//     }
//   };
//   xhr.send();
// }


// function setColorBack(rowIndex, colIndex) {
//   return table
//     .eq(rowIndex)
//     .find("td")
//     .eq(colIndex)
//     .find("button")
//     .css("background-color", "#efefef");
// }


// const errorColor = "#ff0000"
// const correctColor = "#38b000"
// const correct = [[0,0], [3,2], [3,1], [3,0], [2,3], [5,1]]