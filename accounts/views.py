from django.shortcuts import redirect, render
from django.contrib import messages, auth
#from django.contrib.auth.models import User
from accounts.models import *

def register(request):
    if request.method == 'POST':
        # messages.error(request, "Testing error message")
        # Register User 
        first_name = request.POST["first_name"]
        last_name = request.POST["last_name"]
        username = request.POST["username"]
        password = request.POST["password"]
        email = request.POST["email"]
        password2 = request.POST["password2"]

        # Check is password match 
        if password == password2:
            # Check username
            if teacher_account.objects.filter(username=username).exists():
                messages.error(request, 'Sorry, that username is being used')
                return redirect('register')
            else:
                if teacher_account.objects.filter(email=email).exists():
                    messages.error(request, 'Sorry, that email is being used ')
                    return redirect('register')
                else: 
                    # everything is ok 
                    user = teacher_account(
                        first_name=first_name,
                        last_name=last_name,
                        username=username,
                        password=password,
                        email=email,
                        password2=password2)
                    # Login after register 
                    # auth.login(request, user)
                    # messages.success(request, 'You are now log in')
                    # return redirect('index')

                    user.save()
                    messages.success(request, 'You are now registered and can log in')
                    return redirect('login')
        else:
            messages.error(request, 'Passwords do not match')
            return redirect('register')
    else:
        return render(request, 'accounts/register.html')


def login(request):
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = teacher_account.objects.filter(username=username)
        if user:
            if user[0].password == password:
                context = {}
                context['islogin'] = 'true'
                return render(request, 'accounts/dashboard.html', context)
                # return redirect('dashboard')
            else:
                message.error(request, 'Invcalid credentials')
                return redirect('login')
        else:
            message.error(request, 'Invcalid credentials')
            return redirect('login')
    else:
        return render(request, 'accounts/login.html')


def logout(request):
    return render(request, 'accounts/login.html')


def dashboard(request):

    return render(request, 'accounts/dashboard.html')

